import 'dart:async';

import 'package:bp_alipay/bp_alipay_result.dart';
import 'package:flutter/services.dart';

class BpAlipay {
  static const MethodChannel _channel = const MethodChannel('bp_alipay');

  static Future<BpAlipayResult> pay(String orderInfo) async {
    var result = await _channel.invokeMethod("pay", orderInfo);
    return BpAlipayResult.fromMap(Map<String, dynamic>.from(result));
  }

  static Future<bool> isInstalled() async {
    return await _channel.invokeMethod("isInstalled");
  }
}
