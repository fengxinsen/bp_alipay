class BpAlipayResult {
  String resultStatus;
  String result;
  String memo;
  String platform;

  static BpAlipayResult fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    BpAlipayResult i = BpAlipayResult();
    i.resultStatus = map['resultStatus'];
    i.result = map['result'];
    i.memo = map['memo'];
    i.platform = map['platform'];
    return i;
  }

  Map toJson() => {
        "resultStatus": resultStatus,
        "result": result,
        "memo": memo,
        "platform": platform,
      };

  static List<BpAlipayResult> parse(List<dynamic> json) {
    return json.map((e) => BpAlipayResult.fromMap(e)).toList();
  }
}
